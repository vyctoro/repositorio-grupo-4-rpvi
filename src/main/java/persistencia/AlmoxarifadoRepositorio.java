package persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Almoxarifado;
import model.AlmoxarifadoCentral;
import model.AlmoxarifadoLocal;
import model.CampusEnum;
import model.Produto;
import persistencia.daos.AlmoxarifadoDao;
import persistencia.daos.ProdutoDao;

public class AlmoxarifadoRepositorio {
	
	private static Map<CampusEnum, Almoxarifado> almoxarifados;
	private static AlmoxarifadoDao dao;
	
	public static Map<CampusEnum, Almoxarifado> getAlmoxarifados() {
		if (almoxarifados == null) {
			dao = new AlmoxarifadoDao();
			almoxarifados = new HashMap<>();
			ResultSet r = dao.selectAll();
			
			try {
				while(r.next()) {
					Almoxarifado a = null;
					if (r.getBoolean("iscentral") == true) {
						a = new AlmoxarifadoCentral();
						almoxarifados.put(CampusEnum.CENTRAL, a);
					}
					else {
						for (CampusEnum ce : CampusEnum.values()) {
							if (r.getString("descricao").equals(ce.getNome())) {
								a = new AlmoxarifadoLocal(ce);
								almoxarifados.put(ce,  a);
							}
						}
					}
				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			
			r = new ProdutoDao().selectAllOrderedByCampus();
			
			try {
				while(r.next()) {
					String desc = r.getString("descricao");
					for(CampusEnum a : CampusEnum.values()) {
						if (a.getNome().equals(desc)) {
							almoxarifados.get(a).addProdutoAoEstoque(new Produto(r.getString("nome"), r.getString("pdesc"), r.getInt("quantidade"), r.getInt("id_produto")));
						}
					}
				}
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return almoxarifados;
	}

	public Almoxarifado getAlmoxarifado(CampusEnum campusSelecionado) {
		return almoxarifados.get(campusSelecionado);
	}

}

package persistencia;

import model.Pedido;
import persistencia.daos.PedidoDao;

public class RequisicaoTransferenciaRepositorio {

	private PedidoDao dao;
	
	public RequisicaoTransferenciaRepositorio() {
		dao = new PedidoDao();
	}
	
	public void fazerRequisicao(Pedido pedido) {
		dao.create(pedido);
		
	}

}

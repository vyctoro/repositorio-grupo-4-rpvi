package persistencia.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Produto;
import persistencia.ConnectionFactory;

public class ProdutoDao {

	private Connection conn;
	
	public ProdutoDao() {
		conn = ConnectionFactory.getConnection();
	}
	
	public ResultSet selectAll(){
		
		try {
			PreparedStatement prepareStatement = conn.prepareStatement("SELECT produto.nome, produto.descricao, sum(quantidade) as quantidade, produto.id_produto \r\n" + 
					"FROM almoxarifado_has_produto\r\n" + 
					"INNER JOIN almoxarifado ON almoxarifado.id_almoxarifado = almoxarifado_has_produto.id_almoxarifado\r\n" + 
					"INNER JOIN produto ON produto.id_produto = almoxarifado_has_produto.id_produto\r\n" + 
					"GROUP BY produto.id_produto\r\n" + 
					"\r\n" + 
					"");
			return prepareStatement.executeQuery();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public ResultSet selectAllOrderedByCampus() {

		try {
			PreparedStatement prepareStatement = conn.prepareStatement("SELECT almoxarifado.descricao, produto.nome, produto.descricao as pdesc, quantidade, produto.id_produto FROM almoxarifado_has_produto\r\n" + 
					"JOIN almoxarifado ON almoxarifado_has_produto.id_almoxarifado = almoxarifado.id_almoxarifado\r\n" + 
					"JOIN produto ON almoxarifado_has_produto.id_produto = produto.id_produto\r\n" + 
					"ORDER BY almoxarifado.descricao");
			return prepareStatement.executeQuery();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
}

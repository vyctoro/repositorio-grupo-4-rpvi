package persistencia.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Pedido;
import model.Produto;
import persistencia.ConnectionFactory;

public class PedidoDao {
	
	private Connection conn;
	
	public PedidoDao() {
		conn = ConnectionFactory.getConnection();
	}

	public void create(Pedido p) {
		boolean isFirst = true;
		String sql = "INSERT INTO requisicao_transferencia(id_almoxarifado_origem, id_almoxarifado_destino) VALUES ("
				+ "(SELECT id_almoxarifado FROM almoxarifado WHERE descricao LIKE '"+ p.getPedinte().getCampus().getNome()+"'),"
						+ "(SELECT id_almoxarifado FROM  almoxarifado WHERE descricao LIKE '"+ p.getDestino().getCampus().getNome() +"') ) RETURNING id_requisicao_transferencia;";
		
		/*sql+= "INSERT INTO requisicao_has_produto(id_requisicao, id_produto, qtd) VALUES";
		
	
		for(Produto i : p.getItens()) {
			if (!isFirst) sql+=",";
			sql += "(lastval(), (SELECT id_produto FROM produto WHERE produto.id_produto ="+i.getId()+"), "+i.getQuantidade()+ ")";
			
			isFirst = false;
		}*/
		try {
			PreparedStatement prep = conn.prepareStatement(sql);
			ResultSet r = prep.executeQuery();
			r.next();
			int id = r.getInt("id_requisicao_transferencia");
			
			String sql2 = "INSERT INTO requisicao_has_produto(id_requisicao, id_produto, qtd) VALUES";
			
			
			for(Produto i : p.getItens()) {
				if (!isFirst) sql2+=",";
				sql2 += "("+id+", (SELECT id_produto FROM produto WHERE produto.id_produto ="+i.getId()+"), "+i.getQuantidade()+ ")";
				
				isFirst = false;
			}
			
			prep = conn.prepareStatement(sql2);
			prep.executeQuery();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getSQLState());
		}
	}
}

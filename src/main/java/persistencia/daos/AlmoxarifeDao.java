package persistencia.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Almoxarifado;
import model.AlmoxarifadoCentral;
import model.Almoxarife;
import model.CampusEnum;
import persistencia.AlmoxarifadoRepositorio;
import persistencia.ConnectionFactory;

public class AlmoxarifeDao {

	private Connection conn;
	
	public AlmoxarifeDao() {
		conn = ConnectionFactory.getConnection();
	}
	
	public Almoxarife getAlmoxarife(String login, String senha) {
		Almoxarifado a = null;
		Almoxarife almoxarife = null;
		AlmoxarifadoRepositorio.getAlmoxarifados();
		String sql = "SELECT almoxarifado.descricao, almoxarifado.iscentral FROM almoxarife"
				+ " JOIN almoxarifado ON id_almoxarifado_responsavel = almoxarifado.id_almoxarifado"
				+ " WHERE email LIKE '"+login+"' AND senha LIKE '"+senha+"'";
		
		try {
			PreparedStatement prep = conn.prepareStatement(sql);
			ResultSet r = prep.executeQuery();
			r.next();
			
			String desc = r.getString("descricao");
			boolean isCentral = r.getBoolean("iscentral");
			
			if(isCentral) {
				a = new AlmoxarifadoCentral();
			} else{
				for(CampusEnum ce : CampusEnum.values()) {
					if(ce.getNome().equals(desc)){
						a = new AlmoxarifadoRepositorio().getAlmoxarifado(ce);
					}
				}
			}
			
			almoxarife = new Almoxarife(a);
			
			return almoxarife;
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return null;
	}
}

package persistencia.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import persistencia.ConnectionFactory;

public class AlmoxarifadoDao {

	private Connection conn;

	public AlmoxarifadoDao() {
		conn = ConnectionFactory.getConnection();
	}

	public ResultSet selectAll() {
		try {
			PreparedStatement p = conn.prepareStatement("SELECT * FROM almoxarifado");
			return p.executeQuery();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

}

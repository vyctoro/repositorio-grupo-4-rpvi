package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

import model.sessao.SessaoManager;

public class SessaoFilter implements Filter{

	private SessaoManager sm;
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		sm = new SessaoManager();
		String contextPath = ((HttpServletRequest)request).getContextPath();
		if(sm.isAlguemLogado() == false) {
			((HttpServletResponse)resp).sendRedirect(contextPath + "/login.xhtml");
		}
		chain.doFilter(request, resp);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}

package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.CampusEnum;
import model.sessao.SessaoManager;

public class SessaoCentralFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		SessaoManager sm = new SessaoManager();
		
		String url = ((HttpServletRequest) request).getRequestURL().toString();
		String contextPath = ((HttpServletRequest) request).getContextPath();
		HttpServletResponse res = (HttpServletResponse) resp;

		if (SessaoManager.isUsuarioLogado() == false) {
			res.sendRedirect(contextPath + "/login.xhtml?faces-redirect=true");
		} else if (SessaoManager.getUsuario().getAlmoxarifadoResponsavel().getCampus() != CampusEnum.CENTRAL) {
			res.sendRedirect(contextPath + "/denied.xhtml?faces-redirect=true");
		}  else {
			chain.doFilter(request, resp);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}

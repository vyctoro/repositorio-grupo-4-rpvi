package beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

import model.Almoxarifado;
import model.CampusEnum;
import model.Pedido;
import model.PedidoFactory;
import model.Produto;
import model.sessao.SessaoManager;
import persistencia.AlmoxarifadoRepositorio;
import persistencia.RequisicaoTransferenciaRepositorio;

@ManagedBean
@ViewScoped
public class PedidoBean {

	private DualListModel<Produto> itens;
	private List<Produto> itensDisponiveis;
	private List<Produto> itensSelecionados;
	private Map<String, CampusEnum> campus;
	private CampusEnum campusSelecionado;
	private Map<CampusEnum, Almoxarifado> almoxarifados;

	@PostConstruct
	public void init() {
		campus = new HashMap<>();
		itensDisponiveis = new ArrayList<>();
		itensSelecionados = new ArrayList<>();
		almoxarifados = AlmoxarifadoRepositorio.getAlmoxarifados();

		itens = new DualListModel<>(itensDisponiveis, itensSelecionados);

		for (CampusEnum c : CampusEnum.values()) {
			campus.put(c.getNome(), c);
		}
	}

	public void concluirPedido() {
		SessaoManager sessaoManager = new SessaoManager();
		Almoxarifado pedinte = sessaoManager.getAlmoxarifadoSessao();
		Almoxarifado destino = almoxarifados.get(campusSelecionado);
		Pedido pedido = PedidoFactory.criar(pedinte, destino, itens.getTarget());
		
		new RequisicaoTransferenciaRepositorio().fazerRequisicao(pedido);
		
		FacesMessage msg = new FacesMessage();
		msg.setSummary("Pedido enviado");
		msg.setDetail("Deu tudo certo meu querido! s2");

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void addMessage(String summary) {

	}

	public DualListModel<Produto> getItens() {
		return itens;
	}

	public void setItens(DualListModel<Produto> itens) {
		this.itens = itens;
	}

	public CampusEnum getCampusSelecionado() {
		return campusSelecionado;
	}

	public void setCampusSelecionado(CampusEnum campusSelecionado) {
		this.campusSelecionado = campusSelecionado;
	}

	public List<Produto> getItensDisponiveis() {
		return itensDisponiveis;
	}

	public void setItensDisponiveis(List<Produto> itensDisponiveis) {
		this.itensDisponiveis = itensDisponiveis;
	}

	public List<Produto> getItensSelecionados() {
		return itensSelecionados;
	}

	public void setItensSelecionados(List<Produto> itensSelecionados) {
		this.itensSelecionados = itensSelecionados;
	}

	
	public Map<String, CampusEnum> getCampus() {
		return campus;
	}

	public void setCampus(Map<String, CampusEnum> campus) {
		this.campus = campus;
	}

	public void onTransfer(TransferEvent event) {
		// this.itens.getTarget().add((Item) event.getItems().get(0));
		// this.itens.getTarget().addAll(((Collection<? extends Item>)
		// event.getItems()));
	}
	
	public void onCampusChange() {
		itensDisponiveis = almoxarifados.get(campusSelecionado).getItensEmEstoque();
		itens.setSource(itensDisponiveis);
		itens.getTarget().clear();
	}
}

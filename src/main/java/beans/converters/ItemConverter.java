package beans.converters;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

import model.Produto;
import persistencia.ItemRepositorio;

@FacesConverter(value="itemConverter")
public class ItemConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent comp, String arg2) {
		Object ret = null;
		List<Produto> bagulho = new ItemRepositorio().getAll();
	    if (comp instanceof PickList) {
	        Object dualList = ((PickList) comp).getValue();
	        DualListModel dl = (DualListModel) dualList;
	        for (Object o : dl.getSource()) {
	            String id = "" + ((Produto) o).getId();
	            if (arg2.equals(id)) {
	                ret = o;
	                break;
	            }
	        }
	        if (ret == null)
	            for (Object o : dl.getTarget()) {
	                String id = "" + ((Produto) o).getId();
	                if (arg2.equals(id)) {
	                    ret = o;
	                    break;
	                }
	            }
	    }
	    return ret;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		// TODO Auto-generated method stub
		return String.valueOf(((Produto) arg2).getId());
	}
	
}

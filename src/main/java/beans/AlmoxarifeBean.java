package beans;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import model.AlmoxarifadoCentral;
import model.Almoxarife;
import model.sessao.SessaoManager;

@ManagedBean
@SessionScoped
public class AlmoxarifeBean {

	private SessaoManager sessaoManager;
	private Almoxarife usuario;
	private MenuModel menuCabecalho;
	private String login;
	private String senha;
	
	@PostConstruct
	public void init() {
		sessaoManager = new SessaoManager();
		

		
	}

	public String logar() {
		sessaoManager.logar(login, senha);
		usuario = sessaoManager.getUsuarioLogado();
		gerarMenuCabecalho();
		
		return "local_central/solicitacoes?faces-redirect=true";
		
	}
	
	public String sair() {
		sessaoManager.sair();
		return "/login?faces-redirect=true";
	}
	
	
	private void gerarMenuCabecalho() {
		menuCabecalho = new DefaultMenuModel();

		if (usuario.getAlmoxarifadoResponsavel() instanceof AlmoxarifadoCentral) {
			DefaultMenuItem botaoAtenderReposicoesMensais = new DefaultMenuItem("Atender reposições mensais");
			botaoAtenderReposicoesMensais.setUrl("#");
			
			menuCabecalho.addElement(botaoAtenderReposicoesMensais);
		} else {
			DefaultMenuItem botaoSolicitarItens = new DefaultMenuItem("Solicitar itens");
			botaoSolicitarItens.setUrl("/local/pedido.xhtml");
			DefaultMenuItem botaoRegistrarItens = new DefaultMenuItem("Registrar entrada de produto");
			botaoRegistrarItens.setUrl("#");
			
			menuCabecalho.addElement(botaoSolicitarItens);
			menuCabecalho.addElement(botaoRegistrarItens);
		}
		DefaultMenuItem botaoAnalisarSolicitacoes = new DefaultMenuItem("Analisar solicitações");
		botaoAnalisarSolicitacoes.setUrl("/local_central/solicitacoes.xhtml");
		menuCabecalho.addElement(botaoAnalisarSolicitacoes);
		DefaultMenuItem botaoConsultarEstoques = new DefaultMenuItem("Consultar estoques");
		botaoConsultarEstoques.setUrl("#");

		DefaultMenuItem botaoSair = new DefaultMenuItem("Sair");
		botaoSair.setCommand("#{almoxarifeBean.sair}");
		menuCabecalho.addElement(botaoSair);
		
		menuCabecalho.generateUniqueIds();
		
	}

	public Almoxarife getUsuario() {
		return usuario;
	}

	public void setUsuario(Almoxarife usuario) {
		this.usuario = usuario;
	}

	public MenuModel getMenuCabecalho() {
		return menuCabecalho;
	}

	public void setMenuCabecalho(MenuModel menuCabecalho) {
		this.menuCabecalho = menuCabecalho;
	}

	public SessaoManager getSessaoManager() {
		return sessaoManager;
	}

	public void setSessaoManager(SessaoManager sessaoManager) {
		this.sessaoManager = sessaoManager;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	

}

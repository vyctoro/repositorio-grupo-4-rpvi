package model;

public class Produto {

	private int quantidade;

	private String nome;

	private String descricao;
	
	private int id;
	
	public Produto(String nome, String descicao, int quantidade, int id) {
		this.nome = nome;
		this.descricao = descicao;
		this.quantidade = quantidade;
		this.id = id;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	public String getNome() {
		return this.nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Produto [quantidade=" + quantidade + ", nome=" + nome + ", descricao=" + descricao + ", id=" + id + "]";
	}
	
	

}

package model;

import java.util.List;

public class AlmoxarifadoLocal extends Almoxarifado {

	public AlmoxarifadoLocal(CampusEnum campus) {
		this.campus = campus;
	}
	
	public void fazerPedidoMensalParaCentral(List<Produto> itens) {
		AlmoxarifadoCentral central = new AlmoxarifadoCentral();
		Pedido pedido = PedidoFactory.criar(this, central, itens);
		
	}
	
	public CampusEnum getCampus() {
		return this.campus;
	}
	
	
}

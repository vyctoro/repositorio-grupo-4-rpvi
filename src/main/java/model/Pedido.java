package model;

import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.util.List;

public class Pedido {

	private LocalDateTime data;
	private List<Produto> itens;
	private Almoxarifado pedinte;
	private Almoxarifado destino;
	
	public Almoxarifado getPedinte() {
		return pedinte;
	}
	public void setPedinte(Almoxarifado pedinte) {
		this.pedinte = pedinte;
	}
	public Almoxarifado getDestino() {
		return destino;
	}
	public void setDestino(Almoxarifado destino) {
		this.destino = destino;
	}
	public LocalDateTime getData() {
		return data;
	}
	public List<Produto> getItens() {
		return itens;
	}
	
	public void adicionarItem(Produto item) {
		if (item.getQuantidade() < 1) {
			throw new InvalidParameterException("A quantidade reqisitda deve ser maior do que 0");
		}
		this.itens.add(item);
	}
	
	public void setItens(List<Produto> itens) {
		this.itens = itens;
	}
	
}

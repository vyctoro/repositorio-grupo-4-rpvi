package model.sessao;

import javax.faces.context.FacesContext;

import model.Almoxarifado;
import model.AlmoxarifadoLocal;
import model.Almoxarife;
import model.CampusEnum;
import persistencia.daos.AlmoxarifeDao;

public class SessaoManager {

	private static final String USUARIO = "usuario";
	private static boolean isUsuarioLogado = false;
	private static Almoxarife usuario;

	public void logar(String login, String senha) {
		Almoxarife temp = new AlmoxarifeDao().getAlmoxarife(login, senha);
		isUsuarioLogado = true;
		this.usuario = temp;
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(SessaoManager.USUARIO, temp);
	}
	
	public Almoxarifado getAlmoxarifadoSessao() {
		return ((Almoxarife) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(USUARIO)).getAlmoxarifadoResponsavel();
	}

	public Almoxarife getUsuarioLogado() {
		return (Almoxarife) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(USUARIO);
	}
	
	public CampusEnum getCampusSessao() {
		return ((Almoxarife) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(USUARIO)).getAlmoxarifadoResponsavel().getCampus();
	}

	public void sair() {
		usuario = null;
		isUsuarioLogado = false;
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
		
	}
	
	public boolean isAlguemLogado() {
		return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey(USUARIO);
	}

	public static boolean isUsuarioLogado() {
		return isUsuarioLogado;
	}

	public static Almoxarife getUsuario() {
		return usuario;
	}
	
	
}

package model;

public enum CampusEnum {

	ALEGRETE("Campus Alegrete"), BAGE("Campus Bag�"), CACAPAVA_DO_SUL("Campus Ca�apava do Sul"), DOM_PEDRITO("Campus Dom Pedrito"), 
	ITAQUI("Campus Itaqui"), JAGUARAO("Campus Jaguar�o"), SANTANA_DO_LIVRAMENTO("Campus Santana do Livramento"),
	SAO_BOARJA("Campus S�o Borja"), SAO_GABRIEL("Campus S�o Gabriel"), URUGUAIANA("Campus Uruguaiana"), CENTRAL("Almoxarifado Central");
	
	private String nome;
	
	public String getNome() {
		return this.nome;
	}
	
	CampusEnum(String nome) {
		this.nome = nome;
	}
	
}

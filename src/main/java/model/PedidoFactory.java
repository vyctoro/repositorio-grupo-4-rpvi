package model;

import java.util.List;

import persistencia.RequisicaoTransferenciaRepositorio;

public class PedidoFactory {

	
	public static Pedido criar(Almoxarifado pedinte, Almoxarifado destino, List<Produto> produtos) {
		Pedido pedido = new Pedido();
		pedido.setPedinte(pedinte);
		pedido.setDestino(destino);
		pedido.setItens(produtos);
		return pedido;
		
	}
}

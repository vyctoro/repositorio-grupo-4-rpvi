package model;

import java.util.ArrayList;
import java.util.List;

public class Almoxarifado {

	private List<Produto> itensEmEstoque;
	private List<Pedido> pedidosRecebidos;
	protected CampusEnum campus;

	
	
	public Almoxarifado() {
		this.itensEmEstoque = new ArrayList<>();
	}

	public int getQuantidadeItemEmEstoque(String nomeItem) {
		for (Produto item : itensEmEstoque) {
			if (item.getNome().equals(nomeItem)) {
				return item.getQuantidade();
			}
		}
		
		return 0;
	}
	
	public void receberPedido(Pedido pedido) {
		this.pedidosRecebidos.add(pedido);
	}
	
	public CampusEnum getCampus() {
		return this.campus;
	}
	
	public void setCampus(CampusEnum campus) {
		this.campus = campus;
	}

	public List<Produto> getItensEmEstoque() {
		return itensEmEstoque;
	}

	public void addProdutoAoEstoque(Produto produto) {
		this.itensEmEstoque.add(produto);
	}

	@Override
	public String toString() {
		return "Almoxarifado [itensEmEstoque=" + itensEmEstoque.size() + ", pedidosRecebidos=" + pedidosRecebidos + ", campus="
				+ campus.getNome() + "]\n\n";
	}
	
	
	
}
